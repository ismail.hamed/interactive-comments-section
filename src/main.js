import { createApp, defineAsyncComponent } from "vue";

// import router from "./router.js";
// import store from "./store/index.js";
import App from "./App.vue";
import BaseCard from "./components/ui/BaseCard.vue";
import BaseButton from "./components/ui/BaseButton.vue";
import BaseBadge from "./components/ui/BaseBadge.vue";
import BaseVote from "./components/ui/BaseVote.vue";
import BaseAvatar from "./components/ui/BaseAvatar.vue";

const BaseDialog = defineAsyncComponent(() =>
  import("./components/ui/BaseDialog.vue")
);

const app = createApp(App);

app.component("base-card", BaseCard);
app.component("base-button", BaseButton);
app.component("base-badge", BaseBadge);
app.component("base-dialog", BaseDialog);
app.component("base-vote", BaseVote);
app.component("base-avatar", BaseAvatar);
app.mount("#app");
